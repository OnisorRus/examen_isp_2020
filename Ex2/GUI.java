
import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame implements ActionListener{
    JTextField firstNumber = null;
    JTextField secondNumber = null;
    JTextField result=null;
    JButton button = null;

    public GUI() throws Exception {
        setSize(330, 110);
        setTitle("Multiply");
        setVisible(true);
        getContentPane().setLayout(null);
        Initializer();
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void Initializer() {
        this.firstNumber = new JTextField();
        this.firstNumber.setBounds(5, 5, 100, 25);
        this.firstNumber.setVisible(true);

        this.secondNumber= new JTextField();
        this.secondNumber.setBounds(110,5,100,25);
        this.secondNumber.setVisible(true);

        this.result = new JTextField();
        this.result.setBounds(220,5,100,25);
        this.result.setEditable(false);
        this.result.setVisible(true);

        this.button = new JButton("Multiply");
        this.button.setBounds(10, 40, 300, 30);
        this.button.setVisible(true);
        this.button.addActionListener(this);


        add(firstNumber);
        add(secondNumber);
        add(result);
        add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(firstNumber.getText().equals("") || secondNumber.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Introduce numbers!","",JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            String firstNumberText = firstNumber.getText();
            double firstNumber=Double.parseDouble(firstNumberText);
            String secondNumberTest =secondNumber.getText();
            double secondNumber=Double.parseDouble(secondNumberTest);
            double resultNumber=firstNumber*secondNumber;
            String resultNumberText= new String(String.valueOf(resultNumber));
            result.setText(resultNumberText);
        }
    }
}